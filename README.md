![输入图片说明](images/6.jpg)
<h4>✨ 须知</h4>
<p>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;此仓库为启山智软推出的社区团购开源项目仅为后端学习使用，开源不易，点点Star⭐，如咨询其他电商系统或社区团购前端代码请联系下方客服。
</p>
<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
  <thead>
    <tr>
      <th  width="300px" align="center" height="60">系统产品</th>
     <th  width="300px" align="center" height="60">开源状态</th>
    </tr>
  </thead>
  <tbody>
  <tr>
      <td align="center">B2C单商户商城</td>
      <td align="center"> <b><a  href="https://gitee.com/qisange/basemall" target="_blank">开源版本</a></b></td>
    </tr>
 <tr>
      <td align="center">社区团购</td>
      <td align="center"> <b>此仓库开源</b></td>
    </tr>
     <tr>
      <td align="center">O2O外卖商城</td>
      <td align="center"> <b><a  href="https://www.73app.cn/" target="_blank">商业版咨询</a></b></td>
    </tr>
 <tr>
      <td align="center">B2B批发商城</td>
      <td align="center"> <b><a  href="https://www.73app.cn/" target="_blank">商业版咨询</a></b></td>
    </tr>
 <tr>
      <td align="center">
     S2B2C供应链商城
      </td>
   <td align="center">
 <b><a  href="https://www.73app.cn/" target="_blank">商业版咨询</a></b>
      </td>
    </tr>
    <tr>
      <td align="center">B2B2C多商户商城</td>
      <td align="center"> <b><a  href="https://www.73app.cn/" target="_blank">商业版咨询</a></b></td>
    </tr>
  </tbody>
</table>
### 前言
启山智软社区团购系统是一款 **系统稳定** 且经过 **线上反复论证** 并拥有 **大量真实用户** 使用的 Java 社区团购配送系统。

基于市场的反馈和变化，我们在不断开发完善社区团购的基础上，增加属于我们自己的物流配送模块，来帮助线下门店针商品系统下单，批量出单，合理分配，精准配送，在投放各大门店使用后，针对实际情况中出现的各种问题，我们不断的改进，收获了大家的好评和喜欢。

我们由衷希望启山智软社区团购系统可以通过 Gitee 平台让更多的人了解到好的产品。同时欢迎大家积极交流沟通，如有不足之处多给我们的项目提意见或建议，实现共同进步。
|交流群：Smart shop商城|商业咨询|
|---|---|
|![输入图片说明](%E7%A4%BE%E5%8C%BA%E5%9B%A2%E8%B4%AD%E4%BA%A4%E6%B5%81%E7%BE%A4.png)|![输入图片说明](%E5%BE%AE%E4%BF%A1.jpg)|
|QQ群号：556731103   |VX号:18158554030   |
|   |   |
<h4>✨项目地址</h4>
<h5>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;点击链接，你将发现更多的惊喜！</h5>
<h5>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;单店演示
<a  href="https://group.bgniao.cn/copartner/1.0.1/sign" target="_blank">https://group.bgniao.cn/copartner/1.0.1/sign</a>
</h5>
<h5>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;由于账号密码不定期更新，为了方便访问请联系客服，还有O2O丶S2B2C丶B2C丶B2B等更多商业模式等您咨询👇

### 项目介绍

启山智软社区团购是基于  **Spring Cloud 和 Vue.js 的 Java**  系统。包含总控制后台 、城市合伙人/商家pc端、团长/区域团长/提货点后台 、用户端小程序 、H5端等多个操作模块。为响应用户需求我们新增了后台 DIY 装修拖拽式组件，淘宝商品 CSV 一键导入，还有与众不同的管理台侧边栏设计，可支持二开，私有化部署，需求功能定制。

### 荣誉资质
|![输入图片说明](https://images.gitee.com/uploads/images/2021/0908/150857_e5b71af2_8533008.png "微信图片_社区团购资质证书1.png")|![输入图片说明](https://images.gitee.com/uploads/images/2021/0908/151131_413737cf_8533008.png "微信图片_布谷鸟资质证书.png")|![输入图片说明](https://images.gitee.com/uploads/images/2021/0814/104233_f71a9b70_8533008.png "外观专利.png")|
|---|---|---|

### 技术架构
#### 技术选型
| 技术                   | 说明                 | 官网                                                 |
| ---------------------- | -------------------- | ---------------------------------------------------- |
| Spring Cloud           | 微服务框架           | https://spring.io/projects/spring-cloud              |
| Spring Cloud Alibaba   | 微服务框架           | https://github.com/alibaba/spring-cloud-alibaba      |
| Spring Boot            | 容器+MVC框架         | https://spring.io/projects/spring-boot               |
| MyBatis-Plus           | 数据层代码生成       | http://www.mybatis.org/generator/index.html          |
| Swagger                | 文档生成工具         | https://swagger.io/     
|                                                                                                     |
| RabbitMq               | 消息队列             | https://www.rabbitmq.com/                            |
| Redis                  | 分布式缓存           | https://redis.io/                                    |
| Druid                  | 数据库连接池         | https://github.com/alibaba/druid                     |
| OSS                    | 对象存储             | https://github.com/aliyun/aliyun-oss-java-sdk        |
| JWT                    | JWT登录支持          | https://github.com/jwtk/jjwt                         |
| XXL-JOB                | 分布式任务调度平台   |https://www.xuxueli.com/xxl-job/                       |
|                                                                                                     |
| Lombok                 | 简化对象封装工具     | https://github.com/rzwitserloot/lombok               |
| Docker                 | 应用容器引擎         | https://www.docker.com/                              |          
|Sonarqube				 | 代码质量控制	        |https://www.sonarqube.org/
|                                                                                                     |
| element                | 组件库         | https://element.eleme.cn/#/zh-CN                           |
| Vue.js                 | 渐进式JavaScript 框架       | https://cn.vuejs.org/                         |
| Vue-router 			 | 前端路由 		       | https://router.vuejs.org/zh/ 	                            |
| vuex 					 | 状态管理            | https://vuex.vuejs.org/zh/ 								|
| modeuse-core 			 | 自主开发UI组件       | -- 													|
| TypeScript             | JavaScript超集       | https://www.tslang.cn/                    
|                        |
| eslint             	 | 代码质量控制         | https://eslint.org/                                   |                 
| hook	             	 | 代码质量控制         |

#### 系统架构图
![](https://bgniao-small-file-1253272780.cos.ap-chengdu.myqcloud.com/group_purchase_open/sasasa1.png "布谷鸟社区团购架构图")

#### 相关链接
商家后台: https://group.bgniao.cn/copartner/1.0.1/sign

区域团长后台: https://group.bgniao.cn/areaColonel/overview

### 项目演示
| 小程序演示 | 操作流程 |
|-------|------|
|![](images/dfa38c178d478e0e970fe2e3530255d.jpg)|![](https://medusa-small-file-1253272780.cos.ap-shanghai.myqcloud.com/gitee/5051.gif "启山智软社区团购操作流程")      |

### 角色说明
![]( https://bgniao-small-file-1253272780.cos.ap-chengdu.myqcloud.com/group_purchase_open/platform.png "角色说明")
#### 用户端小程序页面展示
![](https://images.gitee.com/uploads/images/2021/1123/110110_34c159a5_8533008.jpeg "未标题.jpg")
#### 商家端页面展示
![输入图片说明](images/gitee.png)

### 典型客户案例
####  中石化
> 基于疫情影响和油费的上涨，再加上电动汽车市场的抨击，中国石化发展了自己的第二事业，想要发动所有店面员工售卖日需品，以店员为团长发动本小区和加油的客户注册会员并团购产品享有超值优惠价，不间断的小程序直播推广，一上线便覆盖两千多名团长，十几万用户量。

| ![输入图片说明](images/123.jpg)  |![输入图片说明](images/456.jpg)   |  ![输入图片说明](images/789.jpg) | ![输入图片说明](images/101112.jpg)  |
|---|---|---|---|

####  良田拾度
> 公司业务线覆盖整个餐饮行业包含前期的原材料种植到菜品生成，基于公司现有业务的发展和新形态的需求，该公司技术员联系到我们需要一套社区团购产品，以线上社区团购模式进行售卖，现以售卖胚芽米为主打产品，结合自有业务需求在短短一个月自行开发了功能并发布上线，上线后成功招募上千名分销员并推广开来，在本地运营的小有名气。

|  ![输入图片说明](images/111.jpg) |![输入图片说明](images/222.jpg)   | ![输入图片说明](images/333.jpg)  |![输入图片说明](images/444.jpg)   |
|---|---|---|---|

## [更新详细说明](https://www.73app.cn/) https://www.73app.cn/

